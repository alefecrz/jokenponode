# Jokenpo Node

# Sobre o App

A ideia deste programa é:

"Um jogo de jokenpô contra o computador"

# Porque?

Aprender o que é uma CLI, um programa de linha de comando, como importar módulos Node e finalmente criamos um Jokenpô bem simples.
Atravez do curso fullstack com Node.js, Bancos de dados, Express e React

Segue o link: 
https://woliveiras.com.br/curso/do-zero-ao-fullstack-com-nodejs-bancos-de-dados-express-e-react/

# Observação sobre essa aplicação

1 - O game não está exatamento igual ao do curso, apliquiei uma lógica diferente para identificar quando o Jogador ganha e perde.

# Getting Started

### Prerequisitos

Para rodar este projeto no modo de desenvolvimento, you precisará ter instalado o Nodejs, que pode ser encontrado [aqui](https://nodejs.org/en/).

# Instalando

### Clonando o repositório

```sh
$ git clone https://gitlab.com/alefecrz/jokenponode.git
$ cd jokenpoNode
```

# Rodando

```sh
$ node index.js
```
